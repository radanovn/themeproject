<?php

use Illuminate\Database\Seeder;
use App\CampaignStatus;

class CampaignsStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new CampaignStatus;
        $status->alias = 'sent';
        $status->name = 'Изпратнеа';
        $status->save();
    }
}
