<?php

use App\CampaignTemplate;
use Illuminate\Database\Seeder;

class CampaignsTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $template = new CampaignTemplate;
        $template->name = 'Template1';
        $template->view = 'Template1';
        $template->save();

        $template = new CampaignTemplate;
        $template->name = 'Template2';
        $template->view = 'Template2';
        $template->save();

        $template = new CampaignTemplate;
        $template->name = 'Template3';
        $template->view = 'Template3';
        $template->save();
    }
}
