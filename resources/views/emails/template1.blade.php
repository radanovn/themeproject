<!DOCTYPE html>
<html>

<head>
    <title>weband.bg</title>
</head>

<body>
    This is template 1

    <h1>Hello</h1>

    <p>Subject: {{ $details['subject'] }}</p>

    <p>Button text: {{ $details['button_text'] }}</p>
    <p>Button color: {{ $details['button_color'] }}</p>


    <p>Button color: {{ $details['logo'] }}</p>




    <p></p>
    <p>{{ $details['text'] }}</p>

    <p>Thank you,
        Radanov.</p>
</body>

</html>


@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}

{{ $details['name'] }}
{{ $details['text'] }}


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('Weband All rights reserved.')
@endcomponent
@endslot
@endcomponent