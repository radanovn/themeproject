<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class NewMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $details;
    public function __construct(Request $request)
    {
        $this->details = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->details->template == '1') {
            return $this->from('from@example.com')
                ->view('emails.template1');
        } else if ($this->details->template == '2') {
            return $this->from('from@example.com')
                ->view('emails.template2');
        } else if ($this->details->template == '3') {
            return $this->from('from@example.com')
                ->view('emails.template3');
        }

        return $this->from('from@example.com')
        ->view('emails.mail');
    }
}
