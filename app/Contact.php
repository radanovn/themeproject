<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'first_name', 'last_name', 'email'];

    // public function newCollection(array $models = [])
    // {
    //     return new ContactCollection($models);
    // }

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault([
            'name' => '-'
        ]);
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    public function fullname()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
