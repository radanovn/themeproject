<?php

namespace App\Imports;

use App\Contact;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Auth;

class ContactsImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        return new Contact([
            'user_id' => Auth::user()->id,
            'first_name' => $row[0],
            'last_name' => $row[1],
            'email' => $row[2]
        ]);
    }
}
